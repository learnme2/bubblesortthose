import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {

        int [] bubbleArray = {15, 17 , -14, -5, 4, 3, 8, 40, -40};

        bubbleSort(bubbleArray);
        System.out.println(Arrays.toString(bubbleArray));
        
    }

    private static void bubbleSort(int[] bubbleArray) {
        for (int i = bubbleArray.length - 1; i > 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (bubbleArray[j] > bubbleArray[j + 1]) {
                    swap(bubbleArray, j, j + 1);
                }
            }
        }
    }

    public static void swap(int[] array, int i, int j) {

        if (i == j) {
            return;
        }

        int temp = array[i];
        array[i]= array[j];
        array[j] = temp;
    }
}


